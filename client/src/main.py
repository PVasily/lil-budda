import asyncio
import webbrowser

import websockets


async def main():
    while True:
        try:
            async with websockets.connect("ws://localhost:8000/websocket") as websocket:
                while True:
                    message = await websocket.recv()
                    webbrowser.open_new_tab(message)
        except Exception as e:
            print(e)
        await asyncio.sleep(5)

if __name__ == '__main__':
    asyncio.run(main())