mypy
flake8

pydantic~=2.5
fastapi~=0.105
uvicorn~=0.25
websockets~=12.0