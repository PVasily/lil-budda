from typing import List

import uvicorn
from fastapi import FastAPI, Body
from fastapi.websockets import WebSocket, WebSocketDisconnect
from pydantic import BaseModel


class ConnectionManager:
    def __init__(self) -> None:
        self.active_connections: List[WebSocket] = []

    async def connect(self, websocket: WebSocket) -> None:
        self.active_connections.append(websocket)

    async def disconnect(self, websocket: WebSocket) -> None:
        self.active_connections.remove(websocket)

    async def broadcast(self, message: str) -> None:
        for connection in self.active_connections:
            await connection.send_text(message)


application = FastAPI()
connection_manager = ConnectionManager()


@application.post("/send")
async def send_message(message: str = Body()):
    await connection_manager.broadcast(message)


@application.websocket("/websocket")
async def websocket_endpoint(connection: WebSocket):
    await connection.accept()
    await connection_manager.connect(connection)
    try:
        while True:
            await connection.receive()
    except WebSocketDisconnect:
        print("DIsconnected")
        await connection_manager.disconnect(connection)


if __name__ == '__main__':
    uvicorn.run(application, host="0.0.0.0", port=80)